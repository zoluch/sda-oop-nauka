package circle;

public class Circle {

    public static double field(double radius) {
        double field = Math.pow(radius, 2) * Math.PI;
        return field;
    }

    public static double circuit(double radius) {
        double circuit = 2*radius*Math.PI;
        return circuit;
    }
}
