package circle;

import java.util.Scanner;

public class CircleApp {
    public static void main(String[] args) {
        System.out.println("Podaj promień okręgu: ");
        Scanner scan = new Scanner(System.in);
        double radius = scan.nextInt();
        scan.nextLine();

        System.out.println("Pole okręgu o promieniu " + radius + " wynosi " + Circle.field(radius));
        System.out.println("Obwód okręgu o promieniu " + radius + " wynosi " + Circle.circuit(radius));
    }
}