package calculator;
import java.util.ArrayList;
import java.util.List;

public class Calculator {
    private List<Double> numbers;

    Calculator() {
        numbers = new ArrayList<Double>();
    }

    public List<Double> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<Double> numbers) {
        this.numbers = numbers;
    }

    public void addNumbers(Double number) {
        numbers.add(number);
    }

    public double sum(List<Double> numbers) {
        double sum = 0;
        for(Double element : numbers)
            sum += element;
        return sum;
    }
    
    public double difference(List<Double> numbers) {
        double difference = numbers.get(0);

        for(int i = 1; i < numbers.size(); i++)
            difference -= numbers.get(i);
        return difference;
    }
    
   public double modulo(List<Double> numbers) {
        double modulo = numbers.get(0);
        for(int i = 1; i < numbers.size(); i++)
            modulo %= numbers.get(i);
        return modulo;
    }
    
    public double multicipation(List<Double> numbers) {
        double multicipation = 1;
        for(Double element : numbers)
            multicipation *= element;
        return multicipation;
    }

    public double division(List<Double> numbers) {
        double division = numbers.get(0);
        for(int i = 1; i < numbers.size(); i++)
            division /= numbers.get(i);
        return division;
    }
}