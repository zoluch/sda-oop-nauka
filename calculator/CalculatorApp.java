package calculator;

import java.util.Scanner;

public class CalculatorApp {
    public static void main(String[] args) {
        System.out.println("Wpisz liczbę liczb do operacji: ");
        int amountOfNumbers;
        Scanner scan = new Scanner(System.in);
        amountOfNumbers = scan.nextInt();
        scan.nextLine();
        if(amountOfNumbers<2) {
            System.out.println("Musisz podać co najmniej 2 liczby");
            amountOfNumbers = scan.nextInt();
            scan.nextLine();
        }

        Calculator numbers = new Calculator();
        System.out.println("Wypisz liczby: ");
        for(int i = 0; i < amountOfNumbers; i++) {
            numbers.addNumbers(scan.nextDouble());
            scan.nextLine();
        }
        System.out.println(numbers.getNumbers());
        System.out.println(numbers.sum(numbers.getNumbers()));
        System.out.println(numbers.difference(numbers.getNumbers()));
        System.out.println(numbers.multicipation(numbers.getNumbers()));
        System.out.println(numbers.division(numbers.getNumbers()));
        System.out.println(numbers.modulo(numbers.getNumbers()));
    }
}
