package points;

public class Point2D {
    private float pointX;
    private float pointY;

    public Point2D() {
    }

    public Point2D(float pointX, float pointY) {
        this.pointX = pointX;
        this.pointY = pointY;
    }

    public void setPointX(float pointX) {
        this.pointX = pointX;
    }

    public void setPointY(float pointY) {
        this.pointY = pointY;
    }

    public void setPointsXY(float pointX, float pointY) {
        this.pointX = pointX;
        this.pointY = pointY;
    }

    public float getPointX() {
        return pointX;
    }

    public float getPointY() {
        return pointY;
    }

    public String toString() {
        return "Point2D{" +
                "pointX=" + pointX +
                ", pointY=" + pointY +
                '}';
    }
}
