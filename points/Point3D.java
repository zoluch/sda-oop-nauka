package points;

public class Point3D extends Point2D {
    private float pointZ;

    public Point3D() {
    }

    public Point3D(float pointX, float pointY, float pointZ) {
        super(pointX, pointY);
        this.pointZ = pointZ;
    }

    public float getPointZ() {
        return pointZ;
    }

    public void setPointZ(float pointZ) {
        this.pointZ = pointZ;
    }

    @Override
    public String toString() {

        return "Point3D{" +
                "pointX=" + getPointX() +
                ", pointY=" + getPointY() +
                ", pointZ=" + pointZ +
                '}';
    }

    public void setPointsXYZ(float pointX, float pointY, float pointZ) {
        setPointsXY(pointX, pointY);
        this.pointZ = pointZ;
    }
}
