package points;

public class PointsApp {
    public static void main(String[] args) {

        Point2D first2DPoint = new Point2D(3.0f, -3.3f);

        Point2D second2DPoint = new Point2D();
        second2DPoint.setPointX(22.74f);
        second2DPoint.setPointY(-300.32f);

        Point2D third2DPoint = new Point2D();
        third2DPoint.setPointsXY(0.33f, 0.23f);

        Point3D point3DFirst = new Point3D(0.0f, 0.25f, -0.003f);

        Point3D point3DSecond = new Point3D();
        point3DSecond.setPointX(16.664f);
        point3DSecond.setPointY(-22.32f);
        point3DSecond.setPointZ(-3.00f);

        Point3D point3DThird = new Point3D();
        point3DThird.setPointsXYZ(-11.0f, -155.33f, 420.0f);

        float pointX = first2DPoint.getPointX();
        float pointY = first2DPoint.getPointY();
        System.out.printf("X = %s\n Y = %s\n", pointX, pointY);
        System.out.println(second2DPoint.toString());
        System.out.println(third2DPoint.toString());

        pointX = point3DFirst.getPointX();
        pointY = point3DFirst.getPointY();
        float pointZ = point3DFirst.getPointZ();
        System.out.printf("X = %s\nY = %s\nZ = %s\n", pointX, pointY, pointZ);
        System.out.println(point3DSecond.toString());
        System.out.println(point3DThird.toString());

    }
}
