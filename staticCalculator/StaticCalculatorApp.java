package staticCalculator;

import java.util.Scanner;

public class StaticCalculatorApp {

    public static void main(String[] args) {
        System.out.println("Wpisz liczbę liczb do operacji: ");
        int amountOfNumbers;
        Scanner scan = new Scanner(System.in);
        amountOfNumbers = scan.nextInt();
        scan.nextLine();
        if (amountOfNumbers < 2) {
            System.out.println("Musisz podać co najmniej 2 liczby");
            amountOfNumbers = scan.nextInt();
            scan.nextLine();
        }
        double[] usersNumbers = new double[amountOfNumbers];
        for (int i = 0; i < amountOfNumbers; i++) {
            System.out.println("Podaj liczbę: ");
            usersNumbers[i] = scan.nextDouble();
        }
        System.out.println("Wybierz cyfrę od 1 do 4, aby wykonać działanie na liczbach(jakakolwiek inna liczba, żeby przerwać):\n1.Suma\n2.Różnica\n3.Iloczyn\n4.Iloraz");
        int usersInput = scan.nextInt();
        scan.nextLine();
        do {
            switch (usersInput) {
                case 1:
                    System.out.println("Suma = " + StaticCalculator.sum(usersNumbers));
                    break;
                case 2:
                    System.out.println("Różnica = " + StaticCalculator.difference(usersNumbers));
                    break;
                case 3:
                    System.out.println("Iloczyn = " + StaticCalculator.product(usersNumbers));
                    break;
                case 4:
                    System.out.println("Iloraz = " + StaticCalculator.quotient(usersNumbers));
                    break;
            }
            System.out.println("Wybierz cyfrę od 1 do 4, aby wykonać działanie na liczbach:\n1.Suma\n2.Różnica\n3.Iloczyn\n4.Iloraz");
            usersInput = scan.nextInt();
            scan.nextLine();
        } while (usersInput == 1 || usersInput == 2 || usersInput == 3 || usersInput == 4);
    }
}
