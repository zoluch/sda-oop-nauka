public class Human {
    String name;
    String surname;
    int height;
    short age;

    Human() {
    }
    Human(String name, String surname, int height, short age) {
        this.name = name;
        this.surname = surname;
        this.height = height;
        this.age = age;
    }
    
    void setName(String name) {
        this.name = name;
    }
    void setSurname(String surname) {
        this.surname = surname;
    }
    void setHeight(int height) {
        this.height = height;
    }
    void setAge(short age) {
        this.age = age;
    }

    String getName() {
        return this.name;
    }
    String getSurname() {
        return this.surname;
    }
    int getHeight() {
        return this.height;
    }
    short getAge() {
        return this.age;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", height=" + height +
                ", age=" + age +
                '}';
    }
}
