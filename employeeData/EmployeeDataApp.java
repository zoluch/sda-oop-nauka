package employeeData;
import java.io.IOException;
import java.util.Arrays;

public class EmployeeDataApp {

    public static void main(String[] args) throws IOException {

        String textFile = "C:\\Users\\Zolo\\IdeaProjects\\sdaNaukaOOP\\src\\employeeData\\Pracownicy.txt";
        EmployeeData[] employee = new EmployeeData[100];
        EmployeeData.employeeFromFile(textFile, employee);


        System.out.println(EmployeeData.employeeFromFile(textFile, employee));
        System.out.println(EmployeeData.averageSalary(employee, "M", (byte) 1));
        System.out.println(Arrays.toString(employee));

        EmployeeData[] employeeToSave = new EmployeeData[3];
        employeeToSave[0] = new EmployeeData("Przemek", "Dav", 3000, "M", (byte) 1);
        employeeToSave[1] = new EmployeeData("Przemcio", "Kowalski", 2300, "M", (byte) 1);
        employeeToSave[2] = new EmployeeData("Danuta", "Wav", 4000, "K", (byte) 1);
        String filePath = "C:\\Users\\Zolo\\IdeaProjects\\sdaNaukaOOP\\src\\employeeData\\PracownicyZapisani.txt";
        EmployeeData.saveToFile(filePath, employeeToSave);
    }
}