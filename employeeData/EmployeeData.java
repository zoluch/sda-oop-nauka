package employeeData;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

public class EmployeeData implements Serializable {
    private String name;
    private String surname;
    private int salary;
    private String gender;
    private byte section;

    public EmployeeData(String name, String surname, int salary, String gender, byte section) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
        this.gender = gender;
        this.section = section;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public byte getSection() {
        return section;
    }

    public void setSection(byte section) {
        this.section = section;
    }

    @Override
    public String toString() {
        return name + " " + surname + " " + salary + " " + gender + " " + section;
    }

    public static int employeeFromFile(String textFile, EmployeeData[] employee) throws FileNotFoundException {

        int employeeCounter = 0;

        Scanner input = new Scanner(new File(textFile));
        input.useDelimiter("\\s+");

        while (input.hasNext()) {
            String name = input.next();
            String surname = input.next();
            int salary = input.nextInt();
            String gender = input.next();
            byte section = input.nextByte();
            EmployeeData newEmployee = new EmployeeData(name, surname, salary, gender, section);
            employee[employeeCounter] = newEmployee;
            employeeCounter++;
        }
        input.close();

        return employeeCounter;
    }

    public static double averageSalary(EmployeeData[] employee, String gender, byte section) {
        int counter = 0;
        int total = 0;
        for (int i = 0; i < 5; i++) {
            if (employee[i].gender.equals(gender) && employee[i].section == section) {
                total += employee[i].salary;
                counter++;
            }
        }

        double averageSalary = total / counter;
        return averageSalary;
    }

    public static void saveToFile(String fileName, EmployeeData[] employee) throws IOException {
        Files.writeString(Paths.get(fileName), Arrays.toString(employee));
    }

}