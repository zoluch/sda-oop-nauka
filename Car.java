public class Car {
    String type;
    int productionYear;

    public Car() {
    }
    public Car(String type, int productionYear) {
        this.type = type;
        this.productionYear = productionYear;
    }

    void setType(String type) {
        this.type = type;
    }

    void setProductionYear(int productionYear) {
        this.productionYear = productionYear;
    }

    String getType() {
        return this.type;
    }

    int getProductionYear() {
        return this.productionYear;
    }

    @Override
    public String toString() {
        return "Car{" +
                "type='" + type + '\'' +
                ", productionYear=" + productionYear +
                '}';
    }
}
