public class Main {

    public static void main(String[] args) {
        Human man = new Human();
        man.setName("Przemek");
        man.setSurname("Dav");
        man.setHeight(180);
        man.setAge((short) 24);
        String mansName = man.getName();
        String mansSurname = man.getSurname();
        int mansHeight = man.getHeight();
        short mansAge = man.getAge();
        System.out.printf("%s %s %s %s", mansName, mansSurname, mansHeight, mansAge);
        System.out.println();

        Car car = new Car();
        car.setType("Combi");
        car.setProductionYear(1998);

        System.out.println(car.getType() + " " + car.getProductionYear());

        Human man2 = new Human("Przemo", "Dy", 173, (short) 22);
        System.out.println(man2.toString());

        Car car2 = new Car("Sedan", 2004);
        System.out.println(car2.toString());
    }
}
